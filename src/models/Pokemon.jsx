//import { SERVER } from '../Config';
const SERVER = 'http://localhost:3000/api';
const IMGDB = 'https://img.pokemondb.net/sprites/omega-ruby-alpha-sapphire/dex/normal';
const MODEL = 'pokemon';
const COLLECTION = MODEL + 's';
const ID_FIELD = 'id';
const FIELDS = [
    { name: 'nombre', default: '', type: 'text' },
    { name: 'caracter', default: '', type: 'text' },    
   // { name: 'picture', default: '', type: 'text' }    
];

export default class Pokemon {
    constructor(data = {}) {
        this.id = (data.id !== undefined && data.id !== null) ? data.id : 0;
        FIELDS.forEach(el => {
            this[el.name] = (data[el.name] !== undefined && data[el.name] !== null) ? data[el.name] : el.default
        });
    }

    static getTitle() {
        return MODEL + 's';
    }

    static getFields() {
        return FIELDS;
    }

    static getModelName() {
        return MODEL;
    }

    static getCollection() {
        return COLLECTION;
    }

    //getAll demana tots els registres a la API
    static getAll = () => {
        const fetchURL = `${SERVER}/${COLLECTION}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(results => {
                    return results.map(e=> {
                        let img = IMGDB +'/' + e.nombre.toLowerCase() + '.png'
                        e.picture = img;
                        return e;
                    })
                })                
                .then(data => resolve(data))
                .catch(err => {reject([{ error: err }]); });
        });
    };


    //getById demana un registre a la API
    static getById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL)
                .then(results => results.json())
                .then(data => resolve(new Pokemon(data[0])))
                .catch(err => reject([{ error: err }]));
        });
    };


    //deleteById elimina un registre a través de la API
    static deleteById = (itemId) => {
        const fetchURL = `${SERVER}/${COLLECTION}/${itemId}`;
        return new Promise((resolve, reject) => {
            fetch(fetchURL, { method: 'DELETE' })
                .then(resp => resp.json())
                .then(resp => {
                    resolve(resp);
                })
                .catch(err => { reject([{ error: err }]); });
        });
    };



    // Mètode no estàtic, desa objecte actual a BDD
    save = () => {
        return new Promise((resolve, reject) => {
            //si l'objecte no té ID assignem 0
            let idValue = this[ID_FIELD];
            if (!idValue) idValue = 0;

            //eliminem mètodes de "this"
            let that = JSON.parse(JSON.stringify(this));
            //si id=0, eliminem id de that
            if (!idValue) {
                delete that.id;
            }

            //mirem si hi ha dades!
            let hiHaDades=false;
            for (var property in that) {
                if (that[property]){
                    hiHaDades=true;
                    break;
                }
            }
            //si no n'hi ha tornem error
            if (!hiHaDades){
                reject({ error: "nodata" });
                return;
            }
          
            //hi ha dades, seguim
            //decidim mètode per API, PUT modificarà, POST farà nou
            let method = (idValue) ? 'PUT' : 'POST';
            //fem connexió passant dades via JSON
            fetch(`${SERVER}/${COLLECTION}`, {
                method,
                headers: new Headers({ 'Content-Type': 'application/json' }),
                    body: JSON.stringify(that)
                })
                .then(resp => resp.json())
                .then((resp) => resolve(resp))
                .catch(err => reject(err));

        });

    }


}