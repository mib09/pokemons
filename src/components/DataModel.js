import React, { Component } from 'react';
import {  Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class DataModel extends Component {
    
    render() {

        let boto1 = (this.props.okText) ? <Button color="primary" onClick={()=>this.props.action('ok', this.props.parameter)}>{this.props.okText}</Button> : <span></span>;
        let boto2 = (this.props.nokText) ? <Button color="secondary" onClick={()=>this.props.action('nok', this.props.parameter)}>{this.props.nokText}</Button> : <span></span>;
  
        return (
            <Modal isOpen={this.props.visible} >
            <ModalHeader>{this.props.title}</ModalHeader>
            <ModalBody>
                {this.props.body}
            </ModalBody>
            <ModalFooter>
                {boto1}
                {boto2} 
            </ModalFooter>
          </Modal>
        );
    }
}


export default DataModel;