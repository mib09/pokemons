import React from 'react'
import { Redirect } from 'react-router-dom';

import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Row, Col } from 'reactstrap';

export default class Details extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            back:false,
            id:this.props.match.params.id
        }
    }

    render(){
        
        if (this.state.back === true) {
            return <Redirect to={'/' + this.props.model.getCollection()} />
        }

        return(
            <h1>{this.state.id}</h1>    
        )
    }
}