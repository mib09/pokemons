import React from "react";
import { BrowserRouter, Link, NavLink, Switch, Route } from "react-router-dom";
import { Container, Row, Col, Button } from 'reactstrap';


import NavMenu from './components/NavMenu';

import Table from './components/ITable';
import P404 from './components/P404';
import Inici from './components/Inici';
import Details from './components/Details';

import Pokemon from './models/Pokemon';

// importamos css
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.min.css';


export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      id:0
    };
  }

  render() {
    return (
      <BrowserRouter>
      <NavMenu />
        <Container>          
            <Row>
            <Col>
              <Switch>
                <Route exact path="/" component={Inici} />
                <Route exact path="/pokemons" render={() => <Table model={Pokemon} />} />
                <Route exact path="/pokemons/:id" render={(props) => <Details model={Pokemon} {...props}/>} />
                <Route component={P404} /> 
              </Switch>
            </Col>
          </Row>

        </Container>
      </BrowserRouter>
    );

  }

}
